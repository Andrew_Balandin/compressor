#include <stdio.h>

#ifndef COMP_H
#define COMP_H

#define AMOUNT_OF_CHARS 256
#define SIZE_BYTE 8

enum Bool {false, true};
typedef enum Bool BOOL;

typedef struct Char
{
    unsigned char ch; 
    float freq;
    char code[256];
    struct Char* left;
    struct Char* right;
}CHAR;

union CODE 
{
    unsigned char ch;
    struct 
    {
        unsigned short b1:1;
        unsigned short b2:1;
        unsigned short b3:1;
        unsigned short b4:1;
        unsigned short b5:1;
        unsigned short b6:1;
        unsigned short b7:1;
        unsigned short b8:1;
    } byte;
};

//it opens file, returns NULL if errors
FILE* openFile(char name_file[], const char mode[]);

//it adds and deletes file extension
char* addFileExt(const char source[], char dest[], const char extension[]);
char* delFileExt(const char source[], char dest[], const char extension[]);

//sets &syms to psyms, returns amount of symbols with frequency != 0.0
int setAdrCharsToPChars(CHAR* chars, CHAR** p_chars, int N);

//prints tree for debugging
void printTree(CHAR* tree);

//initializes syms array
void initCHARS(CHAR* chars, int N);

//builds tree from psym, returns pointer on root
CHAR* buildTree(CHAR* p_chars[], int N);

//fills array syms with frequency of occurrence, returns size of input file 
long long int analyzeFile(FILE* fp, CHAR chars[]);

//fills leafs of tree (sym.code[]) based on the way
void makeCodes(CHAR* root);

//creates .101 file based on input file(fp_in), returns tail (but not all tail - add)
unsigned char write101FileFromIn(FILE* fp_101, FILE* fp_in, CHAR chars[], int N);

//writes .101 file based on .cmp file(fp_cmp)
void write101FileFromCmp(FILE* fp_101, FILE* fp_cmp, CHAR chars[], int N);

//writes header to .cmp file
void writeHeaderCompFile(FILE* fp_comp, int n_chars, CHAR chars[], unsigned char tail, long long int size_infile);

//writes compressed data to .cmp file
void writeDataToCompFile(FILE* fp_101, FILE* fp_comp);

//packs binary code to byte(char), returns packed byte (char)
unsigned char pack(unsigned char buf[]);

//writes out file, returns true if .cmp file is ok, otherwise returns false
int writeOutFile(FILE* fp_out, FILE* fp_cmp, CHAR* tree, long long int size_outfile, unsigned char tail);

//unpacks byte (char) to array (0/1), returns &buf[0]
unsigned char* unpack(unsigned char ch, unsigned char buf[]);

//reads header from .cmp file, returns true if header is ok, otherwise returns false
BOOL readHeaderCompFile(FILE* fp_comp, int* n_chars, CHAR chars[], unsigned char* tail, long long int* size_outfile);

//free memory from tree
void freeTree(CHAR* tree);


void compress(char file_name_in[]);
void decompress(char file_name_cmp[]);

#endif /* COMP_H */

