#include <stdio.h>
#include <stdlib.h>
#include "comp.h"

void compress(char file_name_in[])
{
    int n_chars;
    unsigned char tail;
    float sum = 0;
    long long int size_infile = 0;
    char file_mame_101[256] = {0};
    char file_mame_cmp[256] = {0};
    FILE* fp_in = NULL;
    FILE* fp_101 = NULL;
    FILE* fp_cmp = NULL;
    CHAR chars[AMOUNT_OF_CHARS];
    CHAR* p_chars[AMOUNT_OF_CHARS];
    CHAR* tree = NULL;
    initCHARS(chars, AMOUNT_OF_CHARS);

    fp_in  = openFile(file_name_in, "rb");
    fp_101 = openFile(addFileExt(file_name_in, file_mame_101, ".101"), "w+b");
    fp_cmp = openFile(addFileExt(file_name_in, file_mame_cmp, ".cmp"), "wb");

    if(!(size_infile = analyzeFile(fp_in, chars)))
    {
        fclose(fp_in);
        fclose(fp_101);
        remove(file_mame_101);
        fclose(fp_cmp);        
        remove(file_mame_cmp);
        printf("File \"%s\" skipped, because it is empty!\n", file_name_in);
        return;
    }
    n_chars = setAdrCharsToPChars(chars, p_chars, AMOUNT_OF_CHARS);
    tree = buildTree(p_chars, n_chars);
    makeCodes(tree);
    
    rewind(fp_in);
    tail = write101FileFromIn(fp_101, fp_in, chars, n_chars);

    rewind(fp_cmp);
    writeHeaderCompFile(fp_cmp, n_chars, chars, tail, size_infile);
    rewind(fp_101);
    writeDataToCompFile(fp_101, fp_cmp);
    
    freeTree(tree);
    fclose(fp_in);
    fclose(fp_101);
    remove(file_mame_101);
    fclose(fp_cmp);
    
    printf("File \"%s\" packed to \"%s\"!\n", file_name_in, file_mame_cmp);
    return;
}

void decompress(char file_name_cmp[])
{
    int n_chars;
    unsigned char tail;
    float sum = 0;
    long long int size_outfile = 0;
    char file_mame_101[256] = {0};
    char file_mame_out[256] = {0};
    FILE* fp_out = NULL;
    FILE* fp_101 = NULL;
    FILE* fp_cmp = NULL;
    CHAR chars[AMOUNT_OF_CHARS];
    CHAR* p_chars[AMOUNT_OF_CHARS];
    CHAR* tree = NULL;
    initCHARS(chars, AMOUNT_OF_CHARS);
    
    fp_cmp  = openFile(file_name_cmp, "rb");
    if(!readHeaderCompFile(fp_cmp, &n_chars, chars, &tail, &size_outfile))
    {
        printf("File \"%s\" skipped, because it is not cmp-file!\n", file_name_cmp);
        return;
    }
    
    fp_101 = openFile(addFileExt(file_name_cmp, file_mame_101, ".101"), "w+b");
    fp_out = openFile(delFileExt(file_name_cmp, file_mame_out, ".cmp"), "wb");
    setAdrCharsToPChars(chars, p_chars, AMOUNT_OF_CHARS);
    tree = buildTree(p_chars, n_chars);
    makeCodes(tree);
    write101FileFromCmp(fp_101, fp_cmp, chars, n_chars);
    rewind(fp_101);
    if(!writeOutFile(fp_out, fp_101, tree, size_outfile, tail))
    {
        freeTree(tree);
        fclose(fp_cmp);
        fclose(fp_101);
        remove(file_mame_101);
        fclose(fp_out);
        remove(file_mame_out);
        printf("File \"%s\" skipped, because it is wrong!\n", file_name_cmp);
        return;
    }
  
    freeTree(tree);
    fclose(fp_cmp);
    fclose(fp_101);
    remove(file_mame_101);
    fclose(fp_out);
    
    printf("File \"%s\" unpacked to \"%s\"!\n", file_name_cmp, file_mame_out);
    return;
}