#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "comp.h"

int main(int argc, char* argv[])
{
    if(argc < 2)
    {
        printf("No files in arguments!\n");
        return -1;
    }
    if(strcmp(argv[1], "-c") == 0 )
        for(int i = 2; i < argc; i++)
            compress(argv[i]);
    else if(strcmp(argv[1], "-d") == 0 )
        for(int i = 2; i < argc; i++)
            decompress(argv[i]);
    else
        for(int i = 1; i < argc; i++)
            compress(argv[i]);
    return 0;
}