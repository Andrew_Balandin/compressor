#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "comp.h"

char signature[4] = "CMP";

void initCHARS(CHAR* chars, int N) {
    for (int i = 0; i < N; i++) {
        chars[i].ch = i;
        chars[i].code[0] = '\0';
        chars[i].freq = 0.0;
        chars[i].left = NULL;
        chars[i].right = NULL;
    }
}

CHAR* buildTree(CHAR* p_chars[], int N) {
    CHAR* temp = (CHAR*) malloc(sizeof(CHAR));
    if (N < 2)//file has only 1 symbol
    {
        temp->left = p_chars[N - 1];
        temp->right = NULL;
        temp->freq = p_chars[N - 1]->freq;
        temp->code[0] = 0;
        return temp;
    }
    temp->freq = p_chars[N - 2]->freq + p_chars[N - 1]->freq;
    temp->left = p_chars[N - 1];
    temp->right = p_chars[N - 2];
    temp->code[0] = 0;
    if (N == 2)
        return temp;
    for (int i = N - 2; i >= 0; i--)//insert temp in p_chars ascend
    {
        p_chars[i + 1] = p_chars[i];
        if (p_chars[i]->freq > temp->freq) {
            p_chars[i + 1] = temp;
            break;
        }
        else if (i == 0) {
            p_chars[i] = temp;
            break;
        }
    }
    return buildTree(p_chars, N - 1);
}

int compare(const void* i1, const void* i2) {
    return ((CHAR*) i1)->freq > ((CHAR*) i2)->freq ? -1 :
           ((CHAR*) i1)->freq == ((CHAR*) i2)->freq ? 0 : 1;
}

long long int analyzeFile(FILE* fp, CHAR chars[]) {
    int ch;
    long long int bytesInFile = 0;
    while ((ch = getc(fp)) != EOF) {
        for (int i = 0; i < AMOUNT_OF_CHARS; i++)
            if (ch == chars[i].ch) {
                chars[i].freq += 1.0;
                bytesInFile++;
                break;
            }
    }
    for (int i = 0; i < AMOUNT_OF_CHARS; i++)
        chars[i].freq = chars[i].freq / (double) bytesInFile;
    qsort(chars, AMOUNT_OF_CHARS, sizeof(CHAR), compare);
    return bytesInFile;
}

void makeCodes(CHAR* root) {
    if (root->left) {
        strcpy(root->left->code, root->code);
        strcat(root->left->code, "0");
        makeCodes(root->left);
    }
    if (root->right) {
        strcpy(root->right->code, root->code);
        strcat(root->right->code, "1");
        makeCodes(root->right);
    }
}

unsigned char write101FileFromIn(FILE* fp_101, FILE* fp_in, CHAR chars[], int N) {
    //rewind(fp_in);
    int ch;
    long long int size_file101 = 0;
    while ((ch = fgetc(fp_in)) != EOF) {
        for (int i = 0; i < N; i++)
            if (chars[i].ch == (unsigned char) ch) {
                fputs(chars[i].code, fp_101);
                size_file101 += strlen(chars[i].code);
                break;
            }
    }
    return size_file101 % SIZE_BYTE ? SIZE_BYTE - size_file101 % SIZE_BYTE : 0;
}

void write101FileFromCmp(FILE* fp_101, FILE* fp_cmp, CHAR chars[], int N) {
    unsigned char buf[SIZE_BYTE + 1] = {0};
    int ch;
    while ((ch = fgetc(fp_cmp)) != EOF) {
        fputs(unpack((unsigned char) ch, buf), fp_101);
    }
}

int setAdrCharsToPChars(CHAR* chars, CHAR** p_chars, int N) {
    int i;
    for (i = 0; i < AMOUNT_OF_CHARS; i++) {
        if (chars[i].freq != 0.0)
            p_chars[i] = &chars[i];
        else
            break;
    }
    return i;
}

char* addFileExt(const char source[], char dest[], const char extension[]) {
    strcpy(dest, source);
    strcat(dest, extension);
    //printf("%s\n", dest);
    return dest;
}

char* delFileExt(const char source[], char dest[], const char extension[]) {
    int len_source = strlen(source);
    if (strcmp(&source[len_source - 4], extension) == 0)
        strncpy(dest, source, len_source - 4);
    else
        addFileExt(source, dest, ".decomp");
    return dest;
}

unsigned char pack(unsigned char buf[]) {
    union CODE code;
    code.byte.b1 = buf[0] - '0';
    code.byte.b2 = buf[1] - '0';
    code.byte.b3 = buf[2] - '0';
    code.byte.b4 = buf[3] - '0';
    code.byte.b5 = buf[4] - '0';
    code.byte.b6 = buf[5] - '0';
    code.byte.b7 = buf[6] - '0';
    code.byte.b8 = buf[7] - '0';
    return code.ch;
}

unsigned char* unpack(unsigned char ch, unsigned char* buf) {
    union CODE code;
    code.ch = ch;
    buf[0] = code.byte.b1 + '0';
    buf[1] = code.byte.b2 + '0';
    buf[2] = code.byte.b3 + '0';
    buf[3] = code.byte.b4 + '0';
    buf[4] = code.byte.b5 + '0';
    buf[5] = code.byte.b6 + '0';
    buf[6] = code.byte.b7 + '0';
    buf[7] = code.byte.b8 + '0';
    return buf;
}

void writeHeaderCompFile(FILE* fp_cmp, int n_chars, CHAR chars[], unsigned char tail, long long int size_infile) {
    fputc(signature[0], fp_cmp);
    fputc(signature[1], fp_cmp);
    fputc(signature[2], fp_cmp); //signature
    fwrite((void*) &n_chars, sizeof(int), 1, fp_cmp); //write number of symbols in infile
    for (int i = 0; i < n_chars; i++)//write table of frequency
    {
        fwrite((void*) &chars[i].ch, sizeof(unsigned char), 1, fp_cmp);
        fwrite((void*) &chars[i].freq, sizeof(float), 1, fp_cmp);
    }
    fwrite(&tail, sizeof(unsigned char), 1, fp_cmp);
    fwrite(&size_infile, sizeof(long long int), 1, fp_cmp);
}

void writeDataToCompFile(FILE* fp_101, FILE* fp_comp) {
    unsigned char buf[SIZE_BYTE] = {0};
    unsigned char i = 0;
    int ch;
    while ((ch = fgetc(fp_101)) != EOF) {
        if (i < SIZE_BYTE) {
            buf[i] = (unsigned char) ch;
            i++;
        }
        else {
            fputc(pack(buf), fp_comp);
            i = 0;
            buf[i] = (unsigned char) ch;
            i++;
        }
    }
    if (i != 0)//exist tail
    {
        for (int j = i; j < SIZE_BYTE; j++)
            buf[j] = '0';
        fputc(pack(buf), fp_comp);
    }
}

FILE* openFile(char name_file[], const char mode[]) {
    FILE* fp = fopen(name_file, mode);
    if (fp == NULL) {
        printf("File: %s\n", name_file);
        perror("Wrong file");
        exit(EXIT_FAILURE);
    }
    return fp;
}

BOOL readHeaderCompFile(FILE* fp_comp, int* n_chars, CHAR chars[], unsigned char* tail, long long int* size_outfile) {
    //check signature
    char buf[4] = {0};
    buf[0] = fgetc(fp_comp);//signature
    buf[1] = fgetc(fp_comp);//signature
    buf[2] = fgetc(fp_comp);//signature
    if (strcmp(buf, signature) != 0)
        return false;
    fread((void*) n_chars, sizeof(int), 1, fp_comp); //read number of symbols in in_file
    for (int i = 0; i < *n_chars; i++)//read table of frequency
    {
        fread((void*) &chars[i].ch, sizeof(unsigned char), 1, fp_comp);
        fread((void*) &chars[i].freq, sizeof(float), 1, fp_comp);
    }
    fread(tail, sizeof(unsigned char), 1, fp_comp);
    fread(size_outfile, sizeof(long long int), 1, fp_comp);
    return true;
}

int writeOutFile(FILE* fp_out, FILE* fp_101, CHAR* tree, long long int size_outfile, unsigned char tail) {
    int ch, i = tail;
    unsigned char buf[SIZE_BYTE] = {0};
    unsigned char deCode(CHAR* root) {
        if (ch == '0') {
            if (root->left->left == NULL && root->left->right == NULL)
                return root->left->ch;
            ch = fgetc(fp_101);
            deCode(root->left);
        }
        else {
            if (root->right->left == NULL && root->right->right == NULL)
                return root->right->ch;
            ch = fgetc(fp_101);
            deCode(root->right);
        }
    }
    while (size_outfile != 0 && (ch = fgetc(fp_101)) != EOF) {
        size_outfile--;
        fputc(deCode(tree), fp_out);
    }
    //check tail of .101 file and size of out file
    while ((ch = fgetc(fp_101)) != EOF)
        i--;
    if (!size_outfile && !i)
        return true;
    else
        return false;
}

void printTree(CHAR* tree)//for debug
{
    if (tree) {
        printTree(tree->left);
        if (tree->left == NULL && tree->right == NULL)
            //if(tree->left == NULL && tree->right == NULL && tree->freq != 0)
            printf("%c - %15s - %f\n", tree->ch, tree->code, tree->freq);
        printTree(tree->right);
    }
}

void freeTree(CHAR* tree) {
    CHAR* pright;
    if (tree != NULL) {
        pright = tree->right;
        freeTree(tree->left);
        if (tree->left != NULL || tree->right != NULL)
            free(tree);
        freeTree(pright);
    }
}